#!/usr/bin/env bash
OLD_DIR=`pwd`
THIS_DIR="$(dirname "$0")"

if [ "$(whoami)" != "root" ]; then
    echo "[Hog VM Setup] FATAL: Script must be run as root."
    exit -1
fi

OSNAME="$(grep '^NAME=' /etc/os-release)"

if [[ $OSNAME == *"CentOS"* ]]; then
    centosversion=`rpm -qa \*-release | grep -Ei "oracle|redhat|centos" | cut -d"-" -f3`
    if [[ $centosversion -lt 7 ]]; then
        echo "[Hog VM Setup] FATAL: This script runs only on CentOS 7 or 8."
        exit -1
    fi
    cd "${THIS_DIR}"

    echo
    echo "[Hog VM Setup] Installing wandisco repository..."
    cp Repos/$centosversion/wandisco-git.repo /etc/yum.repos.d/
    rpm --import http://opensource.wandisco.com/RPM-GPG-KEY-WANdisco
    echo
    echo "[Hog VM Setup] Installing useful packages..."
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | bash
    rpm -Va --nofiles --nodigest
    yum -y install jq zip unzip perl-YAML cvsps perl-CGI perl-DBI subversion-perl cvs tk perl subversion perl-Net-SMTP-SSL xauth gcc-c++ tcllib
    echo "[Hog VM Setup] Updating to recent version of git from wandisco..."
    yum -y update
    rpm -Va --nofiles --nodigest
    yum -y install git
    yum -y --disablerepo=base,updates  update git
    yum -y install bash-completion bash-completion-extras
    git --version
    echo "[Hog VM Setup] Installing gitlab-runner"
    yum -y install gitlab-runner

    echo
    echo "[Hog VM Setup] Installing uhal from ipbus..."
    if [[ $centosversion == 7 ]]; then
        curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.centos7.x86_64.repo -o /etc/yum.repos.d/ipbus-sw.repo
    else 
        curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.centos8.x86_64.repo -o /etc/yum.repos.d/ipbus-sw.repo
        yum-config-manager --enable powertools
    fi
    yum -y groupinstall uhal


    echo
    echo "[Hog VM Setup] Installing EOS Citrine client..."
    cp Repos/$centosversion/xrootd.repo /etc/yum.repos.d/
    cp Repos/$centosversion/eos.repo /etc/yum.repos.d/
    yum -y install xrootd-client eos-client

    # Do a final yum upgrade
    echo
    echo "[Hog VM Setup] Final Yum upgrade..."

    rpm -Va --nofiles --nodigest
    yum -y upgrade

    cd "${OLD_DIR}"
elif [[ $OSNAME == *"AlmaLinux"* ]]; then
    almaversion=`rpm -qa \*-release | grep -Ei "oracle|redhat|centos|almalinux" | cut -d"-" -f3`
    echo "[Hog VM Setup] Installing useful packages..."
    yum -y install jq zip unzip perl-YAML perl-CGI perl-DBI subversion-perl cvs tk perl subversion perl-Net-SMTP-SSL xauth gcc-c++ tcllib
    yum -y install git
    yum -y install bash-completion
    echo "[Hog VM Setup] Installing gitlab-runner"
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | bash
    yum -y install gitlab-runner
    if [[ $almaversion -lt 9 ]]; then
        curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.centos8.repo -o /etc/yum.repos.d/ipbus-sw.repo  
        yum-config-manager --add-repo "https://storage-ci.web.cern.ch/storage-ci/eos/diopside/tag/testing/el-8/x86_64/"
        yum-config-manager --add-repo "https://storage-ci.web.cern.ch/storage-ci/eos/diopside-depend/el-8/x86_64/"
    else
        curl https://ipbus.web.cern.ch/doc/user/html/_downloads/ipbus-sw.el9.repo -o /etc/yum.repos.d/ipbus-sw.repo
        yum-config-manager --add-repo "https://storage-ci.web.cern.ch/storage-ci/eos/diopside/tag/testing/el-9s/x86_64/"
        yum-config-manager --add-repo "https://storage-ci.web.cern.ch/storage-ci/eos/diopside-depend/el-9s/x86_64/"
    fi
    echo "[Hog VM Setup] Installing UHAL"
    yum-config-manager --enable powertools
    yum clean all
    yum groupinstall -y uhal
    echo "[Hog VM Setup] Installing EOS diopside client"
    yum -y install xrootd-client eos-client --nogpgcheck
    # Do a final yum upgrade
    echo
    echo "[Hog VM Setup] Final Yum upgrade..."
    rpm -Va --nofiles --nodigest
    yum -y upgrade
else
    echo "[Hog VM Setup] FATAL: This script runs only on CentOS 7 or 8."
    exit -1
fi




