# Hog Virtual Machine Setup scripts
This repository contains useful scripts to setup an Openstack Virtual Machine at CERN running CentOs7, to run the Hog Continuous Integration.

Instructions on how to setup the Virtual Machine can be found at https://hog.readthedocs.io/en/latest/02-User-Manual/02-Hog-CI/07-Virtual-Machines.html